Experiments on handling row interchangeability with SEL
=======================================================

Directory overview:
-------------------

`instances/` CNFs for our **highly** benchmark set. CNFs for '14 and '16 SAT competition are available via satcompetition.org  
`raw_data/` Raw output of our experiments, as well as scripts and binaries used.  
`README.md`  
`summary.ods` Summary of our experiments in an Open Document Spreadsheet format (readable by Excel or LibreOffice).  
`summary_memout.pdf` Visualized summary of instances that reached memory out in our experiments.  
`summary_solved.pdf` Visualized summary of instances that got solved within resource limits in our experiments.  