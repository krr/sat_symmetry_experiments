#!/bin/bash

cnf=$1
sym="$cnf.sym"

if [ -s $sym ]
then
    /export/home1/NoCsBack/dtai/jodv/breakid/minisat $cnf 2>&1 | awk '
    /symmetries/ {print $5}
    /restarts/ {print $3} 
    /decisions/ {print $3} 
    /conflicts             :/ {print $3}
    /symconflicts/ {print $3}
    /propagations          :/ {print $3} 
    /sympropagations/ {print $3}
    /SATISFIABLE/ {print $1}
    /ERROR/ {print $1}
    ' | tr '\012' ','
else
    echo "nosym"
fi
