Experiments on handling row interchangeability with SEL
=======================================================

Directory overview:
-------------------

`instances/` CNFs with row interchangeability symmetry input files.  
`raw_data/` Raw output of our experiments, as well as scripts and binaries used.  
`summary.ods` Summary of our experiments in an Open Document Spreadsheet format (readable by Excel or LibreOffice).  
`README.md`  