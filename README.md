Evaluation of Symmetric Explanation Learning (SEL) (featuring Glucose 4.0)
=====================================================================

Directory overview:
-------------------

`row-interchangeability/` Experiments on handling row interchangeability with SEL.  
`sel-evaluation/` Experiments comparing Glucose 4.0, BreakID, SEL, Symmetry Propagation (SP) and Symmetrical Learning Scheme (SLS).  
`.gitignore`  
`README`  